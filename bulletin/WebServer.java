
/**
*Auhtors: Livia Stanley and Tyler Zeller
**/

package bulletin;

import java.io.*;
import java.net.*;
import java.util.*;

public final class BulletinBoard
{
	public static void main(String argv[]) throws Exception
	{
		//Set the port number
		int port = 6789;

		//Establish the listen socket
		ServerSocket serverSocket = null;	
		try{
		serverSocket = new ServerSocket(port);
		}catch (IOException e){
		System.out.println("Listen socket not established on 6789");
		}

		//Process BBP service requests in an infinite loop
		while(true){
			//Listen for a TCP connection request
			Socket clientSocket = serverSocket.accept();
			
			//Construct an object to process the BBP request message
			BBPRequest request = new BBPRequest(clientSocket);

			//Create new thread to process the request
			Thread thread = new Thread(request);

			//Start the thread
			thread.start();
		}

	}
}

final class BBPRequest implements Runnable
{
	final static String CRLF = "\r\n";
	Socket socket;

	//Constructor
	public BBPRequest(Socket socket) throws Exception
	{
		this.socket = socket;
	}

	//Implement the run() method of the Runnable interface
	public void run()
	{
	    while(true){
	        try{
		    processRequest();
		}catch(Exception e){
		    System.out.println(e);
		}
	    }
	}

	private void processRequest() throws Exception
	{
		//Get a reference to the socket's input and output streams
		InputStream is = socket.getInputStream();
		DataOutputStream os = new DataOutputStream(socket.getOutputStream());

		//Set up input stream filters
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		//Get the BBP request message
		String requestLine = br.readLine();

		//Extract the method name from the request line
		StringTokenizer tokens = new StringTokenizer(requestLine);
		String method = tokens.nextToken(); //get %method
		switch(method){
			case "%connect":
				break;
			case "%join":
				break;
			case "%post":
				break;
			case "%leave":
				break;
			case "%exit":
				break;
			case "%message":
				break;
			case "%users":
				break;
		}




		//Prepend a "." so that the file request is within the current directory
		fileName =  "." + fileName;

		//Open the requested file
		FileInputStream fis = null;
		boolean fileExists = true;
		try{
			fis = new FileInputStream(fileName);
		}catch (FileNotFoundException e){
			fileExists = false;
		}

		//Construct the response message
		String statusLine = null;
		String contentTypeLine = null;
		String entityBody = null;
		if(fileExists){
			statusLine = "HTTP/1.1 200 OK" + CRLF;
			contentTypeLine = "Content-type: " + contentType(fileName) + CRLF;
		}else{
 			// if the file requested is any type other than a text (.txt) file, report
 			// error to the web client
 			if (!contentType(fileName).equalsIgnoreCase("text/plain")) {
			statusLine = "HTTP/1.1 404 Not Found" + CRLF;
			contentTypeLine = "Content-type: text/html"+ CRLF;
			entityBody = "<HTML>"+
				"<HEAD><TITLE>Not Found</TITLE></HEAD>" + "<BODY>Not Found</BODY></HTML>";
			} else { // else retrieve the text (.txt) file from your local FTP server
 				statusLine = "HTTP/1.1 200 OK" + CRLF;
 				contentTypeLine = "Content-type: text/plain" + CRLF;
 				// create an instance of ftp client
 				FtpClient ftpclient = new FtpClient();

 				// connect to the ftp server
				ftpclient.connect("liv","liv");

 				// retrieve the file from the ftp server, remember you need to
 				// first upload this file to the ftp server under your user
 				// ftp directory
 				ftpclient.getFile(fileName);

 				// disconnect from ftp server
 				ftpclient.disconnect();

 				// assign input stream to read the recently ftp-downloaded file
 				fis = new FileInputStream(fileName);
 			}
		}

		//Send the status line
		os.writeBytes(statusLine);

		//Send the content type line
		os.writeBytes(contentTypeLine);

		//Send back a blank line to indicate the end of the header lines
		os.writeBytes(CRLF);

		//Send the entity body
		if (fileExists){
			sendBytes(fis,os);
			fis.close();
		}else{
			if (!contentType(fileName).equalsIgnoreCase("text/plain")) {
 				os.writeBytes(entityBody);
 			} else {
 				sendBytes(fis, os);
 			}
		}

		//Display the request line
		System.out.println();
		System.out.println("Request:\n----------------");
		System.out.println(requestLine);

		//Get and display the header lines
		String headerLine = null;
		while((headerLine = br.readLine()).length() != 0){
			System.out.println(headerLine);
		}

		//Display the response
		System.out.println();
		System.out.println("Response:\n----------------");
		System.out.print(statusLine);
		System.out.print(contentTypeLine);
		//Only display entity body if Not Found
		if(!fileExists){
			if (!contentType(fileName).equalsIgnoreCase("text/plain")) {
				System.out.println(entityBody);
			}
		}

		//Close Streams and socket
		os.close();
		br.close();
		socket.close();
	}

	private static void sendBytes (FileInputStream fis, OutputStream os) throws Exception
	{
		//Construct a 1K buffer to hold bytes on their way to the socket
		byte[] buffer = new byte[1024];
		int bytes = 0;

		//Copy requested file into the socket's output stream
		while((bytes = fis.read(buffer)) != -1){
			os.write(buffer, 0, bytes);
		}
	}

}
